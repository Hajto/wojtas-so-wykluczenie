#include <cstdio>
#include "task_wrapper.h"
#include "semaphore_control.h"

int main() {
    printf("Starting Process 2\n");
    int semid = get_shared_semaphore(get_semaphores_count());
    task("Task 21", semid);
    semaphore_unlock(semid,0);
    semaphore_await(semid,1);
    task("Task 22", semid);
    semaphore_unlock(semid,2);
    semaphore_await(semid,3);
    task("Task 23", semid);
    semaphore_unlock(semid, 4);
    return 0;
}