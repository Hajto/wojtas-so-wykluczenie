#include <cstdio>
#include "task_wrapper.h"
#include "semaphore_control.h"

int main() {
    printf("Starting Process 1\n");
    int semid = get_shared_semaphore(get_semaphores_count());
    semaphore_await(semid,2);
    task("Task 11", semid);
    task("Task 12", semid);
    semaphore_unlock(semid, 3);
    return 0;
}