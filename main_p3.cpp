#include <cstdio>
#include "task_wrapper.h"
#include "semaphore_control.h"

int main() {
    printf("Starting Process 3\n");
    int semid = get_shared_semaphore(get_semaphores_count());
    semaphore_await(semid, 0);
    task("Task 31", semid);
    task("Task 32", semid);
    semaphore_unlock(semid, 1);
    semaphore_await(semid, 4);
    task("Task 33", semid);
    return 0;
}