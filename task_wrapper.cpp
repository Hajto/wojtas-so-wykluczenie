#include <iostream>
#include "semaphore_control.h"
#include <fstream>
#include <zconf.h>

void task(const char* description, int semid){
    sleep(1);
    semaphore_await(semid, 5);
    std::cout << description << std::endl;
    std::ofstream myfile;
    myfile.open ("example.txt", std::ios::app);
    if(myfile.is_open()){
        myfile << description <<"\n";
        myfile.close();
    } else {
        std::cout<<"\n Something went terribly wrong file failed to open"<<std::endl;
    }

    semaphore_unlock(semid, 5);
}

int get_semaphores_count(){
    return 6;
}