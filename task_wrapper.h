#ifndef WOJTAS_SEMAFORY_TASK_WRAPPER_H
#define WOJTAS_SEMAFORY_TASK_WRAPPER_H

void task(const char* description, int semid);
int get_semaphores_count();

#endif //WOJTAS_SEMAFORY_TASK_WRAPPER_H
