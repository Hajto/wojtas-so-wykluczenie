#include <iostream>
#include <zconf.h>
#include "semaphore_control.h"
#include <unistd.h>

const static std::string files[] = {"proces1", "proces2", "proces3"};

void spawnChild(const std::string &program) {
    switch(fork()){
        case 0:
            execl(program.c_str(),"","",NULL);
            break;
        case -1:
            printf("Failed to execl");
            exit(1);
        default:
            break;
    }
}

void spawnChildren(){
    std::string path = get_path() + "/";
    for(int i = 0; i < 3; i++){
        spawnChild(path + files[i]);
    }
}

int main() {
    int semid = init_semaphore(6);
    spawnChildren();
    for(int i = 0; i < 3; i++){
        int code = 0;
        int result = wait(&code);

        printf("Proces %d with pid %d exited with status code: %d\n", i, result, code);
    }
    clear_sempahores(semid);
    return 0;
}